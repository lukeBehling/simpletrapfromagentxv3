#
# Warning: you may need more libraries than are included here on the
# build line.  The agent frequently needs various libraries in order
# to compile pieces of it, but is OS dependent and we can't list all
# the combinations here.  Instead, look at the libraries that were
# used when linking the snmpd master agent and copy those to this
# file.
#

CC=gcc

OBJS1=k6CustomAgent.o k6SnmpExampleScalars.o k6SnmpExampleNotifications.o
TARGETS=k6CustomAgent

CFLAGS=-I. `net-snmp-config --cflags`
BUILDLIBS=`net-snmp-config --libs`
BUILDAGENTLIBS=`net-snmp-config --agent-libs`

# shared library flags (assumes gcc)
DLFLAGS=-fPIC -shared

all: $(TARGETS)

k6CustomAgent: $(OBJS1)
	$(CC) -o k6CustomAgent $(OBJS1) $(BUILDAGENTLIBS)


clean:
	rm $(OBJS1) $(OBJS1) $(TARGETS)


