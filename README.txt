

This is example conf files and code that is meant to be loaded onto a Toradex SOM running a 5.8 net-snmp agent that has agentx support enabled.

I'm sure there is something missing, but its a good starting point.

NOTE that the snmpdPersistent and snmptrapdPersisent files need to be renamed, to exlcude the 'Persisent' string, and copied to the correct location on the computer(s) that correspond the the persistent data storage for snmpd/snmptrapd
